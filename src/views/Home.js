import React from 'react'
import Header from '../components/Header'
import HomeTop from '../components/HomeTop'
import HomeApi from '../components/HomeApi'
import Carousel from '../components/Carousel'
import UsedBy from '../components/UsedBy'
import Footer from '../components/Footer'
import RoadMap from '../components/RoadMap'
import Chart from '../components/Chart'
import Staff from '../components/Staff'

function Home () {

    return (
        <>
            <Header/>
            <main>
                <section>
                    <HomeTop/>
                </section>
                <section id='products'>
                    <HomeApi/>
                </section>
                <section className="container-carousel">
                    <Carousel/>
                </section>
                <section id='team'>
                    <UsedBy/>
                </section>
                <section id='roadmap'>
                    <RoadMap/>
                </section>
                <section id='tokenization'>
                    <Chart/>
                </section>
                <section id='staff'>
                    <Staff/>
                </section>
            </main>
            <Footer check={true}/>
        </>
    )
}

export default Home

