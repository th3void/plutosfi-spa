import React from 'react'
import {useTranslation} from "react-i18next"
import rmap from '../assets/rmap.png'

// TODO: Links on App Store Icons

function RoadMap() {

    const {t} = useTranslation()
    
    return (
        <div>
            <div className='home-sec-map'>
                <h1 className='roadmap' >{t('roadmap.title')}</h1>
                <div className='home-sec-map-card'>
                    <img  alt='Road map' src={rmap} />                
                </div>
            </div>
        </div>
    )
}

export default RoadMap