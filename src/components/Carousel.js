import React from 'react'
import Carousel from 'react-multi-carousel'
import 'react-multi-carousel/lib/styles.css'
import {useTranslation} from "react-i18next"
import ImgOne from '../assets/fiatOnRamp.svg'
import ImgTwo from '../assets/developer.svg'
import ImgThree from '../assets/non-custodial.svg'

// TODO: Change alt

function ComponentCarousel () {
  
    // CAROUSEL
    const responsive = {
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 3,
          slidesToSlide: 3 // optional, default to 1.
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 3,
          slidesToSlide: 3 // optional, default to 1.
        },
        mobile: {
          breakpoint: { max: 500, min: 0 },
          items: 1,
          slidesToSlide: 1 // optional, default to 1.
        }
      };

    // TRANSLATION
    const {t} = useTranslation()

    return (
      <>
        <Carousel
            className='my-own-custom-container'
            swipeable={false}
            draggable={false}
            showDots={true}
            responsive={responsive}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["tablet", "mobile"]}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
        >
            <div className="carousel-box">
                <img src={ImgTwo} alt="info1"/>
                <h5>{t('carousel.title_one')}</h5>
                <p>{t('carousel.info_one')}</p>
                <ol>
                  <li>•	To ensure a uniform and aesthetically pleasing appearance of the overall waterfall, and to ensure the delivery of core information, both types of social content are modularized.</li>
                  <li>•	Comments are open and have a word limit.</li>
                  <li>•	Authenticated users can post and comment on messages.</li>
                  <li>•	Authenticated users can follow the messages.</li>
                  <li>•	Users who are not certified can only view, not interact or follow.</li>
                  <li><br/><br/><br/></li>
                </ol>
            </div>
            <div className="carousel-box">
                <img src={ImgThree} alt="info1"/>
                <h5>{t('carousel.title_two')}</h5>
                <p>{t('carousel.info_two')}</p>
                <p>
                <ol>
                  <li>• In the asset analysis module, the statistical results of user assets of different time period (by year, by month, by week, and by day) are presented in the form of pie charts and trend charts.</li>
                  <li>• Additionally, users can select external assets in addition to their own wallet assets, and extract and summarize information about user assets by entering their wallet address.</li>
                  <li>• The asset management module is mainly a wallet service, which allows users to deposit/withdraw and exchange their crypto assets.</li>
                  <li>• The asset module can be extended to mainstream cryptocurrency exchanges, fiat currency exchange, and deposit modules.</li>
                </ol>
                </p>
            </div>
            <div className="carousel-box">
                <img src={ImgOne} alt="info1"/>
                <h5>{t('carousel.title_three')}</h5>
                <p>{t('carousel.info_three')}</p>
                <ol>
                  <li>• The user fills out personal information, authentication information, etc., in the personal information section.</li>
                  <li>• Users holding coins are platform nodes for platform revenue distribution.</li>
                  <li>• Task center, where users can receive social tasks (paid by brands or initiated by the community) and receive rewards after completing them.</li>
                  <li>• Backstage, a service that manages opinion leaders, mainly provides informational and signal content and intelligently pushes appropriate content to help opinion leaders produce and publish valuable content quickly.</li>

                </ol>
            </div>
        </Carousel>
        </>
    )
}

export default ComponentCarousel