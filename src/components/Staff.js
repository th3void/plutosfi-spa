import React from 'react'
import {useTranslation} from "react-i18next"
import Edson from '../assets/team/Edson.jpg'
import Leandro from '../assets/team/Leandro.jpg'
import Carlos from '../assets/team/Carlos.png'
import Marcos from '../assets/team/Marcos.png'
import Thales from '../assets/team/Thales.jpg'
import Derick from '../assets/team/Derick.png'
import Caroline from '../assets/team/Carol.jpg'
import Roberto from '../assets/team/Roberto.png'


// TODO: Links on App Store Icons

function Staff() {

    const {t} = useTranslation()
    
    return (
        <div>
            <div className='home-sec-map'>
                    <h1 className='roadmap' >{t('staff.title')}</h1>
                <div className='home-sec-map-card'>
                    <div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <div class=" text-center">
                                        <div class=" cdh cd-border">
                                            <img src={Edson} className='staff-image  mx-auto d-block' alt="Staff-image"/>
                                            <h5 class="-title">{t('staff.name-1')}</h5>
                                            <p class="-text">{t('staff.desc-1')}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class=" text-center">
                                        <div class=" cdh">
                                            <img src={Leandro} className='staff-image  mx-auto d-block' alt="Staff-image"/>
                                            <h5 class="-title">{t('staff.name-2')}</h5>
                                            <p class="-text">{t('staff.desc-2')}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class=" text-center">
                                        <div class=" cdh">
                                            <img src={Carlos} className='staff-image  mx-auto d-block' alt="Staff-image"/>
                                            <h5 class="-title">{t('staff.name-3')}</h5>
                                            <p class="-text">{t('staff.desc-3')}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class=" text-center">
                                        <div class=" cdh">
                                            <img src={Marcos} className='staff-image  mx-auto d-block' alt="Staff-image"/>
                                            <h5 class="-title">{t('staff.name-4')}</h5>
                                            <p class="-text">{t('staff.desc-4')}</p>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm">
                                    <div class=" text-center">
                                        <div class=" cdh">
                                            <img src={Thales} className='staff-image mx-auto d-block' alt="Staff-image"/>
                                            <h5 class="-title">{t('staff.name-5')}</h5>
                                            <p class="-text">{t('staff.desc-5')}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class=" text-center">
                                        <div class=" cdh">
                                            <img src={Derick} className='staff-image mx-auto d-block' alt="Staff-image"/>
                                            <h5 class="-title">{t('staff.name-6')}</h5>
                                            <p class="-text">{t('staff.desc-6')}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class=" text-center">
                                        <div class=" cdh">
                                            <img src={Caroline} className='staff-image mx-auto d-block' alt="Staff-image"/>
                                            <h5 class="-title">{t('staff.name-7')}</h5>
                                            <p class="-text">{t('staff.desc-7')}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class=" text-center">
                                        <div class=" cdh">
                                            <img src={Roberto} className='staff-image mx-auto d-block' alt="Staff-image"/>
                                            <h5 class="-title">{t('staff.name-8')}</h5>
                                            <p class="-text">{t('staff.desc-8')}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>               
                </div>
            </div>
        </div>
        
    )
}

export default Staff