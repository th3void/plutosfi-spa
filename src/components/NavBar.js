import React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

function NavBar() {
  const { t } = useTranslation();

  return (
    <nav className="navbar">
      <a href="#products" className="navbar-link navbar-item-one">
        {t("header.products")}
      </a>
      <a href="#roadmap" className="navbar-link navbar-item-two">
        {t("header.roadmap")}
      </a>
      <a href="#tokenization" className="navbar-link navbar-item-three">
        {t("header.tokeniztion")}
      </a>
      <a href="#staff" className="navbar-link navbar-item-four">
        {t("header.team")}
      </a>
    </nav>
  );
}

export default NavBar;
