import React from 'react'
import {useTranslation} from "react-i18next"
import chart from '../assets/chart.png'

// TODO: Links on App Store Icons

function Chart () {

    const {t} = useTranslation()
    
    return (
        <div>
            <div className='home-sec-map'>
                <div className='home-sec-map-card'>
                    <div class="row">
                        <div class="col-4">
                            <h1 class='chart-title'>{t('chart.title')}</h1>
                            <h3>{t('chart.subtitle')}</h3>
                            <p class='chart-main-paragraph'>
                                {t('chart.text')}
                            </p>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-sm text-justify">
                                    <h1 class='chart-title'>{t('chart.chart-title')}</h1>
                                    <div class="list-token">
                                        <ol>
                                            <li>{t('chart.chart-list-1')}</li>
                                            <li>{t('chart.chart-list-2')}</li>
                                            <li>{t('chart.chart-list-3')}</li><br/>
                                        </ol>
                                    </div>
                                    <div>
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="chart-body">
                                                    <h1 className="chart-title-color-blue">{t('chart.data-title-1')}</h1>
                                                    <h1 className="chart-percent">{t('chart.data-number-1')}</h1>
                                                    <p className="chart-paragraph">
                                                        {t('chart.data-text-1')}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm">
                                                <h1 className="chart-title-color-yellow">{t('chart.data-title-2')}</h1>
                                                <h1 className="chart-percent">{t('chart.data-number-2')}</h1>
                                                <p className="chart-paragraph">
                                                    {t('chart.data-text-2')}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <img  alt='chart' src={chart}/>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm">
                                    <h1 className="chart-title-color-yellow">{t('chart.data-title-3')}</h1>
                                    <h1 className="chart-percent"> {t('chart.data-number-3')} </h1>
                                    <p className="chart-paragraph">
                                        {t('chart.data-text-3')}
                                    </p>
                                </div>
                                <div class="col-sm">
                                    <h1 className="chart-title-color-green">{t('chart.data-title-4')}</h1>
                                    <h1 className="chart-percent"> {t('chart.data-number-4')} </h1>
                                    <p className="chart-paragraph">
                                        {t('chart.data-text-4')}
                                    </p>
                                </div>
                                <div class="col-sm">
                                    <h1 className="chart-title-color-lblue">{t('chart.data-title-5')}</h1>
                                    <h1 className="chart-percent">{t('chart.data-number-5')} </h1>
                                    <p className="chart-paragraph">
                                        {t('chart.data-text-5')}
                                    </p>
                                </div>
                                <div class="col-sm">
                                    <h1 className="chart-title-color-orange">{t('chart.data-title-6')}</h1>
                                    <h1 className="chart-percent"> {t('chart.data-number-6')}</h1>
                                    <p className="chart-paragraph">
                                        {t('chart.data-text-6')}
                                    </p>
                                </div>
                                <div class="col-sm">
                                    <h1 className="chart-title-color-lgray">{t('chart.data-title-7')}</h1>
                                    <h1 className="chart-percent">{t('chart.data-number-7')}</h1>
                                    <p className="chart-paragraph">
                                        {t('chart.data-text-7')}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div> 
    )
}

export default Chart;