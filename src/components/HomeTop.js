import React from 'react'
import { Link } from 'react-router-dom'
import {useTranslation} from "react-i18next"
import Banner from '../assets/robot.jpg'

// TODO: Links on App Store Icons
//     title
// download
// whitpaper
// ask
function HomeTop () {

    const {t} = useTranslation()


    
    return (
        <div className='container home-top'>
            <div className='home-top-information'>
                <h1 >{t('home-top.introduce')}</h1>
                <div>
                <h1 className="page-title">{t('home-top.title')}</h1>
                <p className="lead">{t('home-top.subtitle')}</p>
                </div>
                <div className='download-container-992px'>
                    <div className='download'>
                        <Link>
                            <button class='btn btn-primary'>{t('home-top.whitepaper')}</button>
                        </Link>
                    </div>
                    <div className='download'>
                        <Link>
                            <button class='btn btn-primary'>{t('home-top.ask')}</button>
                        </Link>
                    </div>
                </div>
            </div>
            <div>
                <img className="home-top-Banner" alt='Simulador' src={Banner} />
            </div>
        </div>
    )
}

export default HomeTop;