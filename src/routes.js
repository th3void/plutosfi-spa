import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Home from './views/Home'
import Login from './views/Login'

export default function Routes(){
            
    return(
        <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/login' component={Login}/>
        </Switch>
    )
}